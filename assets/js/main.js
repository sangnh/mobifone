/**
 * Created by TuAnh on 11/5/2014.
 */
$(document).ready(function() {
  //  $("#tag-header").load("tag/header.html");
   // $("#tag-footer").load("tag/footer.html");
   // $("#tag-header-khachhang").load("tag/header-khachhang.html");
   // $("#tag-footer-khachhang").load("tag/footer-khachhang.html");

    $(".edit-form").click(function(e) {
        e.preventDefault();
        var formValue = $(this).parent().parent().find(".form-value");
        if (!formValue.hasClass("opened")) {
            if (!formValue.hasClass("disable")) {
                var inputName = $(this).attr("for");
                formValue.addClass("opened");
                if ($(this).attr("id") == "editPassword") {
                    $(this).parent().remove();
                    $(formValue).attr("colspan", "2");
                    var newHtml = "<div class='row'>" +
                        "<div class='col-sm-6'>" +
                        "<input type='password' name='inputPasswordOld' class='form-control' placeholder='Mật khẩu cũ'>" +
                        "<br>" +
                        "</div>" +
                        "</div>" +
                        "<div class='row'>" +
                        "<div class='col-sm-6'>" +
                        "<input type='password' name='inputPasswordNew' class='form-control' placeholder='Mật khẩu mới'>" +
                        "</div>" +
                        "<div class='col-sm-6'>" +
                        "<input type='password' name='inputPasswordNewRepeat' class='form-control' placeholder='Nhập lại mật khẩu mới'>" +
                        "</div>" +
                        "</div>"
                    $(formValue).html(newHtml);

                } else {
                    formValue.html("<input type='text' class='form-control' name='" + inputName + "' value='" + formValue.html() + "' />");

                }
            }
        }


    })

    $(".form-value").click(function(e) {
        e.preventDefault();
        if ($(this).hasClass("opened")) {

        } else {
            if (!$(this).hasClass("disable")) {
                var formValue = $(this);
                var editForm = $(this).parent().find(".edit-form");
                var inputName = editForm.attr("for");
                formValue.addClass("opened")
                if (editForm.attr("id") == "editPassword") {
                    $(editForm).parent().remove();
                    $(formValue).attr("colspan", "2");
                    formValue.addClass("opened");
                    var newHtml = "<div class='row'>" +
                        "<div class='col-sm-6'>" +
                        "<input type='password' name='inputPasswordOld' class='form-control' placeholder='Mật khẩu cũ'>" +
                        "<br>" +
                        "</div>" +
                        "</div>" +
                        "<div class='row'>" +
                        "<div class='col-sm-6'>" +
                        "<input type='password' name='inputPasswordNew' class='form-control' placeholder='Mật khẩu mới'>" +
                        "</div>" +
                        "<div class='col-sm-6'>" +
                        "<input type='password' name='inputPasswordNewRepeat' class='form-control' placeholder='Nhập lại mật khẩu mới'>" +
                        "</div>" +
                        "</div>"
                    $(formValue).html(newHtml);

                } else {
                    formValue.html("<input type='text' class='form-control' name='" + inputName + "' value='" + formValue.html() + "' />");
                }
            }


        }
    });

    $(".grey-widget.dichvu a").each(function(e) {
        var thisLinkHeight = $(this).height();
        console.log(thisLinkHeight);
        if (thisLinkHeight > 61) {
            $(this).css("padding", "0 19px");
        } else {
            $(this).css("padding", "15px 19px");
        }
    });

    $(window).resize(function(e) {
        $(".grey-widget.dichvu a").each(function(e) {
            var thisLinkHeight = $(this).height();
            console.log(thisLinkHeight);
            if (thisLinkHeight > 61) {
                $(this).css("padding", "0 23px");
            } else {
                $(this).css("padding", "15px 23px");
            }
        });


    });

    $(".xemchitiet").click(function(e) {
        e.preventDefault();
        $(".hidden-content").show();
        var firstChitiet = $(".chitiet-content").first();
        $("body").scrollTop(firstChitiet.offset().top);
    });

    toggleNavContentHTKHTHE = function() {
        $('#nav-huongdan').toggleClass('active');
        $('#nav-hotro').toggleClass('active');
        $('#nav-content-huongdan').toggle();
        $('#nav-content-hotro').toggle();
    };

    $('#nav-huongdan').click(function(e) {
        e.preventDefault();
        if (!$('#nav-huongdan').hasClass('active')) {
            toggleNavContentHTKHTHE();
        }
    });

    $('#nav-hotro').click(function(e) {
        e.preventDefault();
        if (!$('#nav-hotro').hasClass('active')) {
            toggleNavContentHTKHTHE();
        }
    });

    toggleNavContentHTKH = function() {
        $('#nav-the-nganhang').toggleClass('active');
        $('#nav-the-cao').toggleClass('active');
        $('#nav-content-the-nganhang').toggle();
        $('#nav-content-the-cao').toggle();
    };

/*
    $('#nav-the-nganhang').click(function(e) {
        e.preventDefault();
        if (!$('#nav-the-nganhang').hasClass('active')) {
            toggleNavContentHTKH();
        }
    });

    $('#nav-the-cao').click(function(e) {
        e.preventDefault();
        if (!$('#nav-the-cao').hasClass('active')) {
            toggleNavContentHTKH();
        }
    });
*/
//    $('a').on('click touchend', function(e) {
//        var el = $(this);
//        var link = el.attr('href');
//        window.location = link;
//    });


    var boxTitleHeight;
    var lowestBox;

    $(".widget-footer").each(function(i) {
        var boxTitle = $(this).find("p");

        var divHeight = $(boxTitle).css("line-height").replace("px", "");
        if (boxTitle.height() <= divHeight) {
            if ($(this).has("span").length > 0) {
                console.log($(this).find("span").html());
                var descHeight = $(this).find("span").css("height").replace("px", "");
                var descLineHeight = $(this).find("span").css("line-height").replace("px", "");
                console.log(descHeight + " - " + descLineHeight);

                $(this).find("span").css("max-height", descLineHeight * 4 + "px");
                $(this).find("span").css("height", "auto");
                $(this).find("span").css("margin-top", "-8px");
            }

        }
    })

   $(".see-more-button").click(function(e) {
        $("a[aria-expanded='true']").not(this).click();
        // Lấy text của nút và text sau khi ấn ở tham số aria-expended-text để thay đổi khi user click
        var originalText = $(this).text();
        var afterText = $(this).attr("aria-expended-text");
 
        $(this).text(afterText);
        $(this).attr("aria-expended-text", originalText);
 
 
    });
 
    $(".faq-item .see-more-button").click(function(e) {
        $("a[aria-expanded='true']").not(this).click();
        var faq = $(this).parent();
        //var faqelse = $("a[aria-expanded='false']").parent();
        var faqelse = $(this).parent().parent().find("a[aria-expanded='false']").parent();
        $(faqelse).css("background-image", "url(\"/wps/assets/img/dropdown-1.png\")");
        $(faq).css("background-image", "url(\"/wps/assets/img/dropdown-1-up.png\")");
 
    });
 
    $(".see-more-dropdown").click(function(e) {
        var img = $(this).find("img");
 
        var originalText = $(img).attr("src");
        var afterText = $(this).attr("aria-expended-text");
 
        console.log(originalText + " - " + afterText);
 
        $(img).attr("src", afterText);
        $(this).attr("aria-expended-text", originalText);
    })
})